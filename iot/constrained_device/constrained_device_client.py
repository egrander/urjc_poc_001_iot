import socket
import json

from iot.third_party.coapthon.client.helperclient import HelperClient


def main():
    # Init
    host = 'localhost'
    port = 5683
    try:
        tmp = socket.gethostbyname(host)
        host = tmp
    except socket.gaierror:
        pass
    client = HelperClient(server=(host, port))

    # Request available scopes
    response = client.get('/iot/scope')
    # Request id_token
    response = client.post('/iot/token', response.payload)
    id_token = json.loads(response.payload)['id_token']

    # Request available scopes again
    response = client.get('/iot/scope')
    # Request another id_token
    response = client.post('/iot/token', response.payload)
    another_id_token = json.loads(response.payload)['id_token']

    # Request protected resource
    example_event = {'event_id':1, 'event_value': 156638}
    response = client.post('/coap2http', json.dumps(example_event), proxy_uri='http://localhost:5000/events',
                           etag=str(id_token))
    resource_location = json.loads(response.payload)['Location']

    # Get resource with a wrong id_token
    response = client.get('/coap2http', proxy_uri=resource_location, etag=str(another_id_token))
    print('\n\nResponse with wrong id_token')
    print(response.pretty_print())

    # Get resource with a correct id_token
    response = client.get('/coap2http', proxy_uri=resource_location, etag=str(id_token))
    print('\n\nResponse with correct id_token')
    print(response.pretty_print())

    # Stop
    client.stop()


if __name__ == '__main__':
    main()