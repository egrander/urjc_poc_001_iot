from cachetools import TTLCache


class ConstrainedDevicesCtxCache:

    # -- Singleton
    __CtxCache = TTLCache(maxsize=256, ttl=600)

    # -- Static methods

    @staticmethod
    def set(cache_key, cache_value):
        ConstrainedDevicesCtxCache.__CtxCache.__setitem__(cache_key, cache_value)

    @staticmethod
    def get(cache_key):
        return ConstrainedDevicesCtxCache.__CtxCache.get(cache_key)

    @staticmethod
    def pop(cache_key):
        return ConstrainedDevicesCtxCache.__CtxCache.pop(cache_key)

    @staticmethod
    def clear():
        ConstrainedDevicesCtxCache.__CtxCache.clear()
