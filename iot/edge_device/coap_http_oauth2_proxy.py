import argparse
import json
import logging
import requests

from iot.third_party.coapthon.http_proxy.coap_http_proxy import CHProxy
from iot.third_party.coapthon.http_proxy.coap_http_proxy import CoAP_HTTP
from iot.third_party.coapthon.serializer import Serializer
from iot.third_party.coapthon.messages.message import Message
from iot.third_party.coapthon.messages.request import Request
from iot.third_party.coapthon.defines import LOCALHOST, COAP_DEFAULT_PORT, DEFAULT_CH_PATH, Codes, Types
from iot.edge_device.resource.oauth2_resource import get_oauth2_access_token
from iot.edge_device.grant.iot_grant import IoTGrant
from iot.edge_device.resource.hub_config_resource import from_config_file_to_os_environ


logger = logging.getLogger(__name__)

ch_path = DEFAULT_CH_PATH


""" the class that realizes the CoAP-HTTP/HTTPS OAuth2 Proxy """


class CHOAuth2_Proxy(CHProxy):
    """
    This program implements a CoAP-HTTP/HTTPS OAuth2 Proxy.
    It is assumed that URI is formatted like this:
    coap://coap_ip:coap_port/coap2http
        Proxy-Uri: http://remote_ip:remote_port/resource
    You can run this program passing the parameters from the command line or you can use the CHOAuth2_Proxy class in
    your own project.
    """

    def __init__(self, oauth2_config_file, coap_ip=LOCALHOST, coap_port=COAP_DEFAULT_PORT, path=DEFAULT_CH_PATH):
        """
        Initialize the CH OAuth2 proxy.

        :param coap_ip: the ip of the ch_proxy server
        :param coap_port: the port of the ch_proxy server
        :param path: the path of the ch_proxy server

        """
        global ch_path
        ch_path = CHProxy.get_formatted_path(path)
        self.coap_ip = coap_ip
        self.coap_port = coap_port
        server_address = (self.coap_ip, self.coap_port)
        from_config_file_to_os_environ(oauth2_config_file)
        self.coap_server = CoAP_HTTP_OAuth2(server_address)

    def run(self):
        """
        Start the proxy.
        """
        logger.info('Starting CoAP-HTTP/HTTPS OAuth2 Proxy...')
        self.coap_server.listen(10)


""" Overrides CoAP HTTP Forward Proxy """


class CoAP_HTTP_OAuth2(CoAP_HTTP):

    # -- Public methods

    def receive_datagram(self, args):
        """
        Handle messages coming from the udp socket.

        :param args: (data, client_address)
        """
        data, client_address = args

        logging.debug("receiving datagram")

        try:
            host, port = client_address
        except ValueError:
            host, port, tmp1, tmp2 = client_address

        client_address = (host, port)

        serializer = Serializer()
        message = serializer.deserialize(data, client_address)
        if isinstance(message, int):
            self.__generate_and_send_rst_message(client_address, message, message)
            return

        logger.debug("receive_datagram - " + str(message))
        if isinstance(message, Request):
            if message.code == Codes.GET.number and message.uri_path == "iot/scope":
                allowed_scope = IoTGrant.get_allowed_scope(logical_address=host)
                http_response = generate_http_response(200, {'Content-Type': 'application/json'}, allowed_scope)
                # HTTP response to CoAP response conversion
                coap_response = CoAP_HTTP.to_coap_response(http_response, message.code, client_address, message.mid,
                                                           message.token)
                # Send datagram
                self.send_datagram(coap_response)

            elif message.code == Codes.POST.number and message.uri_path == "iot/token":
                json_payload = json.loads(message.payload)
                id_token = IoTGrant.generate_id_token(json_payload['scope'], host)
                http_response = generate_http_response(201, {'Content-Type': 'application/json'}, id_token)

                # HTTP response to CoAP response conversion
                coap_response = CoAP_HTTP.to_coap_response(http_response, message.code, client_address, message.mid,
                                                           message.token)
                # Send datagram
                self.send_datagram(coap_response)

            elif message.code in [Codes.POST.number, Codes.GET.number, Codes.PUT.number, Codes.DELETE.number] and \
                    message.proxy_uri and message.uri_path == "coap2http":
                iot_forwarded_id_token = None
                if len(message.etag) > 0:
                    iot_forwarded_id_token = str(message.etag[0])
                # Verify if id_token is linked with the IoT device context
                if iot_forwarded_id_token and not IoTGrant.verify_id_token(iot_forwarded_id_token, host):
                    self.__generate_and_send_forbidden_message(client_address, message)

                # Execute HTTP/HTTPS request
                http_response = CoAP_HTTP_OAuth2.execute_http_request(message.code, message.proxy_uri, message.payload,
                                                                      iot_forwarded_id_token)
                # HTTP response to CoAP response conversion
                coap_response = CoAP_HTTP.to_coap_response(http_response, message.code, client_address, message.mid,
                                                           message.token)
                # Send datagram
                self.send_datagram(coap_response)

            else:
                self.__generate_and_send_rst_message(client_address, message, Codes.BAD_REQUEST.number)

            # Return
            return

        elif isinstance(message, Message):
            logger.error("Received message from %s", message.source)

        else:  # is Response
            logger.error("Received response from %s", message.source)

    # -- Static methods

    @staticmethod
    def execute_http_request(method, uri, payload, iot_forwarded_id_token=None):
        if iot_forwarded_id_token is not None and len(iot_forwarded_id_token) > 0:
            access_token = get_oauth2_access_token()
            headers = {'Authorization': access_token['token_type'] + ' ' + access_token['access_token'],
                       'IoT-Forwarded': 'Bearer ' + iot_forwarded_id_token}
            return CoAP_HTTP_OAuth2.__get_http_response(method, uri, payload, headers)

        else:
            return CoAP_HTTP_OAuth2.__get_http_response(method, uri, payload)

    # -- Private methods

    # Generate RST message
    def __generate_and_send_rst_message(self, client_address, message, code):
        logger.error("receive_datagram - BAD REQUEST")
        rst = Message()
        rst.destination = client_address
        rst.type = Types["RST"]
        rst.code = code
        rst.mid = message.mid
        self.send_datagram(rst)

    # Generate Forbidden message
    def __generate_and_send_forbidden_message(self, client_address, message):
        logger.error("receive_datagram - Forbidden")
        rst = Message()
        rst.destination = client_address
        rst.type = Types["ACK"]
        rst.code = Codes.FORBIDDEN.number
        rst.mid = message.mid
        self.send_datagram(rst)

    # Get HTTP response
    @staticmethod
    def __get_http_response(method, uri, payload, headers=[]):
        if method == Codes.GET.number:
            return requests.get(url=uri, headers=headers)
        elif method == Codes.DELETE.number:
            return requests.delete(url=uri, headers=headers)
        elif method == Codes.POST.number:
            return requests.post(url=uri, data=payload, headers=headers)
        elif method == Codes.PUT.number:
            return requests.put(url=uri, data=payload, headers=headers)
        else:
            return None


# -- Util methods

def generate_http_response(status_code, headers, content):
    return AttrDict([('status_code', status_code), ('headers', headers), ('content', content)])


# -- Util classes

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


# -- Get command line arguments
def get_command_line_args():
    parser = argparse.ArgumentParser(description='Run the CoAP-HTTP/HTTPS OAuth2 Proxy.')
    parser.add_argument('-ip', dest='coap_ip', default=LOCALHOST,
                        help='the ip of the ch_proxy server')
    parser.add_argument('-cp', dest='coap_port',  type=int, default=COAP_DEFAULT_PORT,
                        help='the port of the ch_proxy server')
    parser.add_argument('-p', dest='path', default=DEFAULT_CH_PATH,
                        help='the path of the ch_proxy server')
    parser.add_argument('-f', dest='oauth2_config_file',
                        help='the path of the OAuth2 config file')
    return parser.parse_args()


if __name__ == "__main__":
    args = get_command_line_args()
    chOAuht2_proxy = CHOAuth2_Proxy(args.oauth2_config_file, args.coap_ip, args.coap_port, args.path)
    chOAuht2_proxy.run()