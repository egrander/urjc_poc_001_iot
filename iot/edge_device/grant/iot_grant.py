import json

from iot.edge_device.grant.model.iot_device import IoTDevice
from iot.edge_device.resource.oauth2_resource import get_allowed_scope, get_oauth2_iot_subtoken
from iot.edge_device.cache.constrained_devices_ctx_cache import ConstrainedDevicesCtxCache


class IoTGrant:

    # -- Global attributes
    __DeviceCtxCache = ConstrainedDevicesCtxCache()

    # -- Static methods

    # Get allowed scope
    @staticmethod
    def get_allowed_scope(logical_address, physical_address=None, user_agent=None, geolocation_x=None,
                          geolocation_y=None, device_id=None):

        # Caching the iot device context
        iot_device = IoTDevice(logical_address, physical_address, user_agent, geolocation_x, geolocation_y, device_id)
        if IoTGrant.__DeviceCtxCache.get(logical_address) is None or \
                not IoTGrant.__DeviceCtxCache.get(logical_address).__eq__(iot_device):
            IoTGrant.__DeviceCtxCache.set(logical_address, iot_device)

        # Return
        return json.dumps({'scope': get_allowed_scope()})

    # Generate id token for the iot device which requested the allowed scopes
    @staticmethod
    def generate_id_token(scope, logical_address, physical_address=None, user_agent=None, geolocation_x=None,
                          geolocation_y=None, device_id=None):

        # Verify the scope requested
        if scope != get_allowed_scope():
            raise ValueError('Inappropriate scope: The requested scope was "' + scope + '"')

        # Verify the iot device context
        iot_device = IoTDevice(logical_address, physical_address, user_agent, geolocation_x, geolocation_y, device_id)
        if IoTGrant.__DeviceCtxCache.get(logical_address) is None or \
                not IoTGrant.__DeviceCtxCache.get(logical_address).__eq__(iot_device):
            raise KeyError('Context not found for the iot device: ' + iot_device.to_json())
        else:
            IoTGrant.__DeviceCtxCache.pop(logical_address)

        # Generate id token
        id_token = get_oauth2_iot_subtoken(iot_device.to_json())

        # Cache the iot device with its id_token
        IoTGrant.__DeviceCtxCache.set(id_token['id_token'], iot_device)

        # Return
        return json.dumps(id_token)

    # Verify if id token is linked with the device context
    @staticmethod
    def verify_id_token(id_token, logical_address, physical_address=None, user_agent=None, geolocation_x=None,
                        geolocation_y=None, device_id=None):
        current_iot_device = IoTDevice(logical_address, physical_address, user_agent, geolocation_x, geolocation_y,
                                       device_id)
        cached_iot_device = IoTGrant.__DeviceCtxCache.get(id_token)
        return False if cached_iot_device is None or not cached_iot_device.__eq__(current_iot_device) else True
