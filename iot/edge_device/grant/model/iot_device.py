import json


class IoTDevice:

    def __init__(self, logical_address, physical_address=None, user_agent=None, geolocation_x=None, geolocation_y=None,
                 device_id=None):
        self.__logical_address = logical_address
        self.__physical_address = physical_address
        self.__user_agent = user_agent
        self.__geolocation_x = geolocation_x
        self.__geolocation_y = geolocation_y
        self.__device_id = device_id

    def __eq__(self, other):
        if self.__logical_address != other.__logical_address or self.__physical_address != other.__physical_address or \
                self.__user_agent != other.__user_agent or self.__geolocation_x != other.__geolocation_x or \
                self.__geolocation_y != other.__geolocation_y or self.__device_id != other.__device_id:
            return False
        return True

    def to_json(self):
        obj = {'logical_address': self.__logical_address}
        if self.__physical_address is not None:
            obj['physical_address'] = self.__physical_address
        if self.__user_agent is not None:
            obj['user_agent'] = self.__user_agent
        if self.__geolocation_x is not None:
            obj['geolocation_x'] = self.__geolocation_x
        if self.__geolocation_y is not None:
            obj['geolocation_y'] = self.__geolocation_y
        if self.__device_id is not None:
            obj['device_id'] = self.__device_id
        return json.dumps(obj)

    # -- Getters

    def get_logical_address(self):
        return self.__logical_address

    def get_physical_address(self):
        return self.__physical_address

    def get_user_agent(self):
        return self.__user_agent

    def get_geolocation_x(self):
        return self.__geolocation_x

    def get_geolocation_y(self):
        return self.__geolocation_y

    def get_device_id(self):
        return self.__device_id
