import ConfigParser
import os


def from_config_file_to_os_environ(file_path):
    config = ConfigParser.ConfigParser()
    if not os.path.exists(file_path) or not os.path.isfile(file_path):
        raise EnvironmentError('File "' + file_path + '" not found')
    config.read(file_path)
    for i in config.items('DEFAULT'):
        os.environ[i[0].upper()] = i[1]

