import logging
from oauthlib.oauth2 import BackendApplicationClient
import os
import requests
from requests.auth import HTTPBasicAuth
from requests_oauthlib import OAuth2Session
import time


logger = logging.getLogger(__name__)

# -- Global attributes

OAUTH2_CLIENT_ID_OS_VAR_NAME = 'OAUTH2_CLIENT_ID'
OAUTH2_CLIENT_SECRET_OS_VAR_NAME = 'OAUTH2_CLIENT_SECRET'
OAUTH2_TOKEN_ENDPOINT_OS_VAR_NAME = 'OAUTH2_TOKEN_ENDPOINT'
OAUTH2_SUBTOKEN_ENDPOINT_OS_VAR_NAME = 'OAUTH2_SUBTOKEN_ENDPOINT'
OAUTH2_ALLOWED_SCOPE_OS_VAR_NAME = 'OAUTH2_ALLOWED_SCOPE'

# -- Private global attributes

__ACCESS_TOKEN = None


# -- Public methods

def get_allowed_scope():
    return __get_os_environ(OAUTH2_ALLOWED_SCOPE_OS_VAR_NAME)


# Get access token (OAuth2 2-legged)
def get_oauth2_access_token():
    global __ACCESS_TOKEN
    if __ACCESS_TOKEN is None or time.time() > __ACCESS_TOKEN['expires_at']:
        # -- Load env variables
        client_id = __get_oauth2_client_id()
        client_secret = __get_oauth2_client_secret()
        allowed_scope = get_allowed_scope()
        token_endpoint = __get_oauth2_token_endpoint()

        # Get access token
        auth = HTTPBasicAuth(client_id, client_secret)
        client = BackendApplicationClient(client_id=client_id, scope=allowed_scope)
        oauth = OAuth2Session(client=client)
        if token_endpoint.startswith("http://"):
            os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
        __ACCESS_TOKEN = oauth.fetch_token(token_url=token_endpoint, auth=auth)

    # Return access token
    return __ACCESS_TOKEN


# Get id token from access token
def get_oauth2_iot_subtoken(iot_device):
    # -- Load env variables
    subtoken_endpoint = __get_oauth2_subtoken_endpoint()
    access_token = get_oauth2_access_token()

    # Get id token token
    headers = {'Authorization': access_token['token_type'] + ' ' + access_token['access_token']}
    response = requests.post(url=subtoken_endpoint, data=iot_device, headers=headers)

    # Return
    return response.json()


# -- Private methods

def __get_oauth2_token_endpoint():
    return __get_os_environ(OAUTH2_TOKEN_ENDPOINT_OS_VAR_NAME)


def __get_oauth2_subtoken_endpoint():
    return __get_os_environ(OAUTH2_SUBTOKEN_ENDPOINT_OS_VAR_NAME)


def __get_oauth2_client_id():
    return __get_os_environ(OAUTH2_CLIENT_ID_OS_VAR_NAME)


def __get_oauth2_client_secret():
    return __get_os_environ(OAUTH2_CLIENT_SECRET_OS_VAR_NAME)


def __get_os_environ(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        logging.error(var_name + ' environment variable is not set.')
        raise LookupError(var_name + ' environment variable is not set.')